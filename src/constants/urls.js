const URLS = {
  // header
  main: "/main",
  news: "/news",
  branches: "/branches",
  theory: "/theory",
  gallery: "/gallery",
  contact: "/contact",
};

export default URLS;
