import EnvelopeOpen from "../assets/icons/EnvelopeOpen.svg";
import EnvelopeClosed from "../assets/icons/EnvelopeClosed.svg";
import TelephonePut from "../assets/icons/TelephonePut.svg";
import TelephonePicked from "../assets/icons/TelephonePicked.svg";
import Logo from "../assets/icons/logo.svg";

const ICONS = {
  Logo,
  // main
  EnvelopeOpen,
  EnvelopeClosed,
  TelephonePut,
  TelephonePicked,
};

export default ICONS;
