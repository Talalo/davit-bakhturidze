import styled from "styled-components";
import ICONS from "../../constants/icons";

export const ArticleWrapper = styled.div`
  width: 100%;
  height: 132px;
  border: 1px solid #9e9e9e21;
  display: flex;
  justify-content: space-between;
  padding: 4px;
  margin-bottom: 16px;
  background: #ffffff;
  border-radius: 4px;
  cursor: pointer;
`;

export const ArticleImage = styled.div`
  min-width: 30%;
  max-width: 220px;
  height: 100%;
  background: url(${({ image }) => image});
  margin-right: 12px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

export const ArticleText = styled.div`
  flex-grow: 1;
  color: #060000bb;
  font-family: Roboto-r;
  overflow: hidden;
  font-size: 14px;
  line-height: 18px;
  padding: 8px 24px 20px 4px;
  position: relative;
  &::after {
    top: 0;
    left: 0;
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
  }
`;

export const ArticleTitle = styled.div`
  color: #060000bb;
  font-weight: 600;
  overflow: hiden;
  font-size: 16px;
  line-height: 18px;
  padding: 0 0 8px 0;
`;

export const ArticleMore = styled.div`
  right: 0;
  bottom: 0;
  content: "";
  position: absolute;
  width: 100%;
  z-index: 333;
  text-align: right;
  color: ${({ theme }) => theme.color.primary};
  padding: 0 8px 2px 8px;
  cursor: pointer;
  background: #ffffff;
  box-shadow: 0px -8px 40px 24px #ffffff;
  &:hover {
    text-decoration: underline;
  }
`;
