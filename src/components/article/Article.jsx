import React, { useState } from "react";
import ICONS from "../../constants/icons";
import URLS from "../../constants/urls";
import { ArticleWrapper, ArticleImage, ArticleText, ArticleMore, ArticleTitle } from "./components";

const Article = ({ image, text }) => {
  return (
    <ArticleWrapper>
      <ArticleImage image={image} />
      <ArticleText>
        <ArticleTitle>სტატიის სათაური</ArticleTitle>
        {text}
        <ArticleMore>ვრცლად...</ArticleMore>
      </ArticleText>
    </ArticleWrapper>
  );
};

export default Article;
