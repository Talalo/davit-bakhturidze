import styled from "styled-components";
import ICONS from "../../constants/icons";

export const HeaderContainer = styled.div`
  width: 100%;
  height: 72px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${({ theme }) => theme.color.primary};
  padding: 0 24px;
  position: fixed;
  z-index: 11111;
  box-shadow: 0px 24px 34px -12px #1b95f88c;
`;

export const HeaderLogo = styled.div`
  min-width: 72px;
  flex-grow: 1;
  height: 72px;
  background-image: url(${ICONS.Logo});
  background-position: left;
  background-size: auto 60%;
  font-family: Gotham-r;
  font-size: 28px;
  color: #e1e6e9;
  padding-left: 60px;
  line-height: 70px;
`;

export const HeaderNav = styled.div`
  display: flex;
  align-items: center;
`;

export const HeaderNavLink = styled.div`
  padding: 8px 0 8px 16px;
  color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 72px;
  position: relative;
  font-size: 16px;
  font-weight: 600;
  cursor: pointer;
  &:hover {
    background: #ffffff22;
  }
  &::after {
    content: "";
    display: ${({ active }) => (active ? "block" : "none")};
    position: absolute;
    width: 100%;
    height: 4px;
    border-radius: 2px;
    background: #ffffff;
    bottom: 0;
    box-shadow: 0px 4px 32px 12px #ffffff88;
  }
`;
