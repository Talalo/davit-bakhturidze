import React, { useState } from "react";
import ICONS from "../../constants/icons";
import URLS from "../../constants/urls";

import { HeaderContainer, HeaderLogo, HeaderNav, HeaderNavLink } from "./components";
import { Link, useLocation } from "react-router-dom";

const Header = () => {
  const { pathname } = useLocation();
  return (
    <HeaderContainer>
      <HeaderLogo>Davit Bakhturidze</HeaderLogo>
      <HeaderNav>
        <Link to={URLS.main}>
          <HeaderNavLink active={pathname === URLS.main}>მთავარი</HeaderNavLink>
        </Link>
        <Link to={URLS.news}>
          <HeaderNavLink active={pathname === URLS.news}>სიახლეები</HeaderNavLink>
        </Link>
        <Link to={URLS.branches}>
          <HeaderNavLink active={pathname === URLS.branches}>მიმართულებები</HeaderNavLink>
        </Link>
        <Link to={URLS.theory}>
          <HeaderNavLink active={pathname === URLS.theory}>თეორია</HeaderNavLink>
        </Link>
        <Link to={URLS.gallery}>
          <HeaderNavLink active={pathname === URLS.gallery}>გალერეა</HeaderNavLink>
        </Link>
        <Link to={URLS.contact}>
          <HeaderNavLink active={pathname === URLS.contact}>კონტაქტი</HeaderNavLink>
        </Link>
      </HeaderNav>
    </HeaderContainer>
  );
};

export default Header;
