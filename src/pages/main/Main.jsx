import React, { useState } from "react";
import ICONS from "../../constants/icons";
import URLS from "../../constants/urls";
import {
  MainContainer,
  MainPageCell,
  ConsultationButton,
  FlexWrapper,
  AuthRegLink,
  MainCellIcon,
  MainCellGmail,
} from "./components";

const Main = () => {
  return (
    <>
      <MainContainer>
        <MainPageCell
          image={"https://skinperfectmedical.com/wp-content/uploads/sites/57/2018/11/botox-men-2-768x720.jpg"}
        />
        <MainPageCell>
          <ConsultationButton>ონლაინ კონსულტაცია</ConsultationButton>
          <FlexWrapper>
            <AuthRegLink>ავტორიზაცია</AuthRegLink>
            <AuthRegLink>რეგისტრაცია</AuthRegLink>
          </FlexWrapper>
        </MainPageCell>
        <MainPageCell
          image={
            "https://lh3.googleusercontent.com/proxy/3ovds4YiO-QzPhLswdmoAi9-D0RGclcOHZl93Uhkh1trfaAr4L0-IdKdSjZXh7o1utIks3aHQf_JTHVPW98P083l7iv1n5UdGRuBhohtHwJnz1NwBLAwmK9AeLZ2HXD3Y0yDZoQGXcQ"
          }
        />
        <MainPageCell>
          <MainCellIcon icon={ICONS.EnvelopeClosed} hoverIcon={ICONS.EnvelopeOpen} />
          <MainCellGmail>somemail@gmail.com</MainCellGmail>
        </MainPageCell>
        <MainPageCell
          image={
            "https://lakecrestdental.com/wp-content/uploads/sites/13/2020/04/Index_MainBanner_E_Mobile-SandSprings.jpg"
          }
        />
        <MainPageCell>
          <MainCellIcon icon={ICONS.TelephonePut} hoverIcon={ICONS.TelephonePicked} />
          <MainCellGmail>{"0 341 22 22 22"}</MainCellGmail>
        </MainPageCell>
      </MainContainer>
    </>
  );
};

export default Main;
