import styled from "styled-components";
import ICONS from "../../constants/icons";

export const MainContainer = styled.div`
  width: 100%;
  height: 100vh;
  padding-top: 72px;
  display: flex;
  flex-wrap: wrap;
`;

export const MainPageCell = styled.div`
  width: 33%;
  min-width: 320px;
  flex-grow: 1;
  height: Calc(50vh - 36px);
  display: flex;
  flex-wrap: wrap;
  background-image: url(${({ image }) => image});
  position: relative;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  &::after {
    content: "";
    position: absolute;
    display: ${({ image }) => (image ? "block" : "none")};
    width: 100%;
    height: 100%;
    background: #000000;
    opacity: 30%;
  }
  &:hover {
    &::after {
      opacity: 20%;
    }
  }
`;

export const ConsultationButton = styled.button`
  max-width: 80%;
  min-width: 60%;
  height: 56px;
  border-radius: 28px;
  background: #31c018;
  color: #ffffff;
  font-size: 20px;
  padding: 0 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border: none;
  outline: none;
  font-weight: 1000;
  &:hover {
    box-shadow: 0px 26px 20px -24px rgb(0 0 0 / 32%);
    transform: scale(1.01);
  }
  &:active {
    opacity: 80%;
  }
`;

export const FlexWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const AuthRegLink = styled.div`
  color: ${({ theme }) => theme.color.primary};
  font-size: 16px;
  margin: 18px 6px;
  font-weight: 600;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;

export const MainCellIcon = styled.div`
  width: 100%;
  height: 32%;
  background-size: contain;
  background-position: center bottom;
  background-image: url(${({ icon }) => icon});
  &:hover {
    background-image: url(${({ hoverIcon }) => hoverIcon});
  }
`;

export const MainCellGmail = styled.div`
  width: 100%;
  padding: 0 42px;
  text-align: center;
  font-size: max(24px, 2.4vw);
  font-family: Roboto-b;
  color: ${({ theme }) => theme.color.primary};
`;
