import styled from "styled-components";
import ICONS from "../../constants/icons";

export const NewsContainer = styled.div`
  width: 100%;
  height: 100vh;
  padding: 108px 16px 32px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const ScrollableContainer = styled.div`
  width: 100%;
  height: 100%;
  /* width */
  &::-webkit-scrollbar {
    width: 4px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.color.neutral};
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.color.primary};
    border-radius: 2px;
  }
  overflow-y: auto;
  padding: 0 24px;
`;
export const NewsColumnTitle = styled.div`
  padding: 16px;
  border-radius: 0 0 4px 4px;
  background: #ffffff;
  position: absolute;
  color: #060000bb;
  font-weight: 600;
  font-size: 18px;
  top: 0;
  left: 24px;
`;

export const NewsLastAdded = styled.div`
  width: 60%;
  padding: 78px 0 12px;
  background: ${({ theme }) => theme.color.neutral};
  border-radius: 6px;
  position: relative;
  height: Calc(100vh - 140px);
`;

export const NewsPopular = styled.div`
  height: Calc(100vh - 140px);
  position: relative;
  width: Calc(40% - 16px);
  padding: 78px 0 12px;
  background: ${({ theme }) => theme.color.neutral};
  border-radius: 6px;
`;
