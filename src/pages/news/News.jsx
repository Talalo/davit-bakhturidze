import React, { useState } from "react";
import ICONS from "../../constants/icons";
import URLS from "../../constants/urls";
import { NewsContainer, NewsLastAdded, NewsPopular, NewsColumnTitle, ScrollableContainer } from "./components";
import Article from "../../components/article/Article";

const News = () => {
  return (
    <NewsContainer>
      <NewsLastAdded>
        <NewsColumnTitle>ბოლოს დამატებული</NewsColumnTitle>
        <ScrollableContainer>
          <Article
            image="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSXiQF1iXgMvcUWayjA1N5n7OjLVEuw8HlFdA&usqp=CAU"
            text="ბავშვს დაბადებისთანავე უკვე ყბებში აქვს განთავსებული 20 სარძევე კბილი. როგორც წესი, ისინი იწყებენ ამოსვლას 6-დან 12 თვემდე შუალედში. როდესაც სარძევე კბილი იწყებს ამოსვლას ზოგიერთ ბავშვს შეიძლება აღენიშნებოდეს ღრძილების შეშუპება და ქავილი. იმისათვის, რომ ბავშვს შევუმსუბუქოთ მდგომარეობა შეიძლება დაუმასაჯოთ ღრძილები სუფთა თითით, პატარა მეტალის კოვზით ან სველი რბილი ნაჭრით. 
          
          რატომ არის სარძევე კბილები ასე მნიშვნელოვანი?"
          />
          <Article
            image="https://scienceblog.cut-e.com/wp-content/uploads/2016/05/Smile-1920x1080.jpg"
            text="Fat son how smiling mrs natural expense anxious friends. Boy scale enjoy ask abode fanny being son. As material in learning subjects so improved feelings. Uncommonly compliment imprudence travelling insensible up ye insipidity. To up painted delight winding as brandon. Gay regret eat looked warmth easily far should now. Prospect at me wandered on extended wondered thoughts appetite to. Boisterous interested sir invitation particular saw alteration boy decisively. "
          />
          <Article
            image="https://nearbydental.com/wp-content/uploads/2018/07/Dental-Braces.jpg"
            text="Fat son how smiling mrs natural expense anxious friends. Boy scale enjoy ask abode fanny being son. As material in learning subjects so improved feelings. Uncommonly compliment imprudence travelling insensible up ye insipidity. To up painted delight winding as brandon. Gay regret eat looked warmth easily far should now. Prospect at me wandered on extended wondered thoughts appetite to. Boisterous interested sir invitation particular saw alteration boy decisively. "
          />
          <Article
            image="https://www.tendersmiles4kids.com/wp-content/uploads/sensitivepaste-Aug2019-920x613.jpg"
            text="ბავშვს დაბადებისთანავე უკვე ყბებში აქვს განთავსებული 20 სარძევე კბილი. როგორც წესი, ისინი იწყებენ ამოსვლას 6-დან 12 თვემდე შუალედში. როდესაც სარძევე კბილი იწყებს ამოსვლას ზოგიერთ ბავშვს შეიძლება აღენიშნებოდეს ღრძილების შეშუპება და ქავილი. იმისათვის, რომ ბავშვს შევუმსუბუქოთ მდგომარეობა შეიძლება დაუმასაჯოთ ღრძილები სუფთა თითით, პატარა მეტალის კოვზით ან სველი რბილი ნაჭრით. 
          
          რატომ არის სარძევე კბილები ასე მნიშვნელოვანი?"
          />
          <Article
            image="https://www.njoralsurgery.com/wp-content/uploads/plantarj-July2019-862x575.jpg"
            text="Fat son how smiling mrs natural expense anxious friends. Boy scale enjoy ask abode fanny being son. As material in learning subjects so improved feelings. Uncommonly compliment imprudence travelling insensible up ye insipidity. To up painted delight winding as brandon. Gay regret eat looked warmth easily far should now. Prospect at me wandered on extended wondered thoughts appetite to. Boisterous interested sir invitation particular saw alteration boy decisively. "
          />
          <Article
            image="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSXiQF1iXgMvcUWayjA1N5n7OjLVEuw8HlFdA&usqp=CAU"
            text="Fat son how smiling mrs natural expense anxious friends. Boy scale enjoy ask abode fanny being son. As material in learning subjects so improved feelings. Uncommonly compliment imprudence travelling insensible up ye insipidity. To up painted delight winding as brandon. Gay regret eat looked warmth easily far should now. Prospect at me wandered on extended wondered thoughts appetite to. Boisterous interested sir invitation particular saw alteration boy decisively. "
          />
        </ScrollableContainer>
      </NewsLastAdded>
      <NewsPopular>
        <NewsColumnTitle>პოპულარული</NewsColumnTitle>
        <ScrollableContainer>
          <Article
            image="https://scienceblog.cut-e.com/wp-content/uploads/2016/05/Smile-1920x1080.jpg"
            text="ბავშვს დაბადებისთანავე უკვე ყბებში აქვს განთავსებული 20 სარძევე კბილი. როგორც წესი, ისინი იწყებენ ამოსვლას 6-დან 12 თვემდე შუალედში. როდესაც სარძევე კბილი იწყებს ამოსვლას ზოგიერთ ბავშვს შეიძლება აღენიშნებოდეს ღრძილების შეშუპება და ქავილი. იმისათვის, რომ ბავშვს შევუმსუბუქოთ მდგომარეობა შეიძლება დაუმასაჯოთ ღრძილები სუფთა თითით, პატარა მეტალის კოვზით ან სველი რბილი ნაჭრით. 
          
          რატომ არის სარძევე კბილები ასე მნიშვნელოვანი?"
          />
          <Article
            image="https://www.tendersmiles4kids.com/wp-content/uploads/sensitivepaste-Aug2019-920x613.jpg"
            text="Fat son how smiling mrs natural expense anxious friends. Boy scale enjoy ask abode fanny being son. As material in learning subjects so improved feelings. Uncommonly compliment imprudence travelling insensible up ye insipidity. To up painted delight winding as brandon. Gay regret eat looked warmth easily far should now. Prospect at me wandered on extended wondered thoughts appetite to. Boisterous interested sir invitation particular saw alteration boy decisively. "
          />
          <Article
            image="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSXiQF1iXgMvcUWayjA1N5n7OjLVEuw8HlFdA&usqp=CAU"
            text="Fat son how smiling mrs natural expense anxious friends. Boy scale enjoy ask abode fanny being son. As material in learning subjects so improved feelings. Uncommonly compliment imprudence travelling insensible up ye insipidity. To up painted delight winding as brandon. Gay regret eat looked warmth easily far should now. Prospect at me wandered on extended wondered thoughts appetite to. Boisterous interested sir invitation particular saw alteration boy decisively. "
          />
        </ScrollableContainer>
      </NewsPopular>
    </NewsContainer>
  );
};

export default News;
