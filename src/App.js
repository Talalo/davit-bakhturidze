import React from "react";
import { ThemeProvider } from "styled-components";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Theme from "./theme";
import Header from "./components/header/Header";
import Main from "./pages/main/Main";
import News from "./pages/news/News";
import URLS from "./constants/urls";

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={Theme}>
        <Header />
        <Switch>
          <Route path={URLS.main} component={Main} />
          <Route path={URLS.news} component={News} />
        </Switch>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
